package me.facundo.youtubeconsumer;

import com.sapher.youtubedl.YoutubeDL;
import com.sapher.youtubedl.YoutubeDLException;
import com.sapher.youtubedl.YoutubeDLRequest;
import com.sapher.youtubedl.YoutubeDLResponse;

import java.text.Normalizer;
import java.util.concurrent.CountDownLatch;

public class YoutubeDownloader {
    private String videoUrl;
    private String path;
    private String filename;
    private YoutubeDLResponse response;
    private CountDownLatch latch = new CountDownLatch(1);

    public YoutubeDownloader(String videoUrl) {
        this.videoUrl = videoUrl;
        this.path = "/";
        this.filename = "NotDone";
    }

    public void download() throws YoutubeDLException {

        // Destination directory
        String directory = System.getProperty("user.home");

        //GET TITLE
        YoutubeDLRequest request = new YoutubeDLRequest(videoUrl, directory);
        request.setOption("ignore-errors");        // --ignore-errors
        // request.setOption("format","'bestaudio[format=\"opus\"]/bestaudio/best'");
        request.setOption("extract-audio");
        request.setOption("audio-format", "mp3");
        request.setOption("retries", 10);// --retries 10
        request.setOption("get-title");
        String title = YoutubeDL.execute(request).getOut();

        //Remove non ascii characters
        title =  Normalizer
                .normalize(title, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
        //Remove trailing "\n"
        title = title.substring(0,title.length() - 1);
        title = title.replaceAll(" ","");

        YoutubeDLRequest request2 = new YoutubeDLRequest(videoUrl,directory);
        request2.setOption("ignore-errors");        // --ignore-errors
        // request.setOption("format","'bestaudio[format=\"opus\"]/bestaudio/best'");
        request2.setOption("extract-audio");
        request2.setOption("audio-format", "mp3");
        request2.setOption("output", title + ".%(ext)s");
        request2.setOption("retries", 10);// --retries 10
        // Make request and return response
        response = YoutubeDL.execute(request2);

        filename = title + ".mp3";
        path = response.getDirectory() + "/" + filename;

        latch.countDown();
    }

    public YoutubeDLResponse getResponse() {
        return response;
    }

    public String getFilename() {
        return filename;
    }

    public String getPath() {
        return path;
    }

    public CountDownLatch getLatch() {
        return latch;
    }
}
