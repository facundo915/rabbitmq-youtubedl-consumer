package me.facundo.youtubeconsumer;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.io.File;

class S3Uploader {
    private String bucketName;
    S3Uploader(String bucketName){
        this.bucketName = bucketName;
    }

    void upload(String filename, String filePath){
        AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.SA_EAST_1).withCredentials(new ProfileCredentialsProvider()).build();
        if (!s3.doesBucketExistV2(bucketName)) {
            s3.createBucket(bucketName);
        }
        try {
            s3.putObject(bucketName, filename, new File(filePath));
        }catch(Exception e){
            e.printStackTrace();
        }
        System.out.println("UPLOADED " + filename + " TO BUCKET " + bucketName);
    }
}
