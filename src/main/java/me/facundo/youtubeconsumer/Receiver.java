package me.facundo.youtubeconsumer;

import com.sapher.youtubedl.YoutubeDLException;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class Receiver {
    private CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessage(String message){
        System.out.println("Recieved <" + message + ">");
        YoutubeDownloader downloader = new YoutubeDownloader(message);
        try {
            downloader.download();
        } catch (YoutubeDLException e) {
            System.out.println("ERROR DOWNLOADING : ");
            System.out.println(downloader.getResponse());
            return;
        }
        try {
            downloader.getLatch().await(20, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }
        System.out.println("Downloaded: " + downloader.getPath());
        S3Uploader uploader = new S3Uploader("rabbit-youtubedl");
        uploader.upload(downloader.getFilename(), downloader.getPath());
        latch.countDown();
    }
    public CountDownLatch getLatch(){
        return latch;
    }
}
